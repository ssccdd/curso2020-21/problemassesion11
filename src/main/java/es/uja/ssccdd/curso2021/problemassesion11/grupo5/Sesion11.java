/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.INGENIEROS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.MAQUINAS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion11 {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = (ExecutorService) Executors.newCachedThreadPool();

        AtomicInteger contIdModelos = new AtomicInteger();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando ingenieros");
        for (int i = 0; i < INGENIEROS_A_GENERAR; i++) {
            executor.execute(new Ingeniero(i, contIdModelos));
        }

        System.out.println("HILO-Principal Generando máquinas");
        for (int i = 0; i < MAQUINAS_A_GENERAR; i++) {
            executor.execute(new MaquinaPostProcesado(i));
        }

        System.out.println("HILO-Principal Espera para parar a los ingenieros");

        try {
            TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion11.class.getName()).log(Level.SEVERE, null, ex);
        }

        executor.shutdownNow();

        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(Sesion11.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

        System.out.println("\n\nHILO-Principal Contador ID modelos: " + contIdModelos);

    }

}
