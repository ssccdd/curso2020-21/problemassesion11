/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.ENFERMEROS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.ALMACENES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion11 {

    public static void main(String[] args) {

        // Variables aplicación
        ExecutorService executor = (ExecutorService) Executors.newCachedThreadPool();

        AtomicInteger contIdPacientes = new AtomicInteger();
        AtomicInteger contIdDosis = new AtomicInteger();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando almacenes");
        for (int i = 0; i < ALMACENES_A_GENERAR; i++) {
            executor.execute(new AlmacenMedico(i,contIdDosis));
        }

        System.out.println("HILO-Principal Generando enfermeros");
        for (int i = 0; i < ENFERMEROS_A_GENERAR; i++) {
            executor.execute(new Enfermero(i, contIdPacientes));
        }

        System.out.println("HILO-Principal Espera para parar a los procesos");

        try {
            TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion11.class.getName()).log(Level.SEVERE, null, ex);
        }

        executor.shutdownNow();

        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(Sesion11.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

      
        System.out.println("\n\nHILO-Principal Contador ID pacientes: " + contIdPacientes);
        System.out.println("HILO-Principal Contador ID dosis: " + contIdDosis);

        
    }

}
