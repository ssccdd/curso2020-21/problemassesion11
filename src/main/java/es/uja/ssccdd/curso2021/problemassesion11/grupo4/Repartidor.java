/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo4;

import es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.TipoPlato;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.BROKER_URL;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.QUEUE;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.MENUS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.TIEMPO_ESPERA_REPARTIDOR;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Repartidor implements Runnable {

    private final int iD;
    private final AtomicInteger contadorIDs;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public Repartidor(int iD, AtomicInteger contadorIDs) {
        this.iD = iD;
        this.contadorIDs = contadorIDs;
    }

    @Override
    public void run() {

        try {
            before();
            task();
        } catch (Exception e) {
            System.out.println("Repartidor " + iD + " ha encontrado una excepción" + e.getMessage());
        } finally {
            after();
        }
    }

    public void before() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
    }

    public void after() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }

    private void task() throws JMSException {
        MessageConsumer consumer = session.createConsumer(destination);
        ArrayList<MenuReparto> listaMenús = new ArrayList<>();
        ArrayList<Plato> platosDescartados = new ArrayList<>();
        boolean interrumpido = false;

        System.out.println("Repartidor " + iD + " ha empezado.");

        for (int i = 0; i < MENUS_A_GENERAR; i++) {
            listaMenús.add(new MenuReparto(contadorIDs.getAndIncrement()));
        }

        while (!interrumpido) {
            try {

                TextMessage mensaje = (TextMessage) consumer.receive();
                System.out.println("Restaurante recibe: " + mensaje.getText());

                String[] mensajeSplit = mensaje.getText().split("-");
                TipoPlato tipo = TipoPlato.getPlatoPorOrdinal(Integer.parseInt(mensajeSplit[0]));
                int id = Integer.parseInt(mensajeSplit[1]);

                Plato plato = new Plato(id, tipo);

                int i = 0;
                boolean insertado = false;
                
                while (i < listaMenús.size() && !insertado) {
                    insertado = listaMenús.get(i).addPlato(plato);
                    i++;
                }
                
                if(!insertado){
                    platosDescartados.add(plato);
                }

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_REPARTIDOR);
            } catch (InterruptedException | JMSException ex) {
                interrumpido = true;

            }
        }
        
        consumer.close();
        imprimirDatos(listaMenús, platosDescartados);

    }

    private void imprimirDatos(ArrayList<MenuReparto> trabajoHecho, ArrayList<Plato> platosDescartados) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nRepartidor ").append(iD).append(" rellenados ").append(trabajoHecho.size()).append(" menús.");

        for (MenuReparto pedido : trabajoHecho) {
            mensaje.append("\n\t\t").append(pedido.toString());
        }

        mensaje.append("\n\tPlatos descartados:");
        for (Plato plato : platosDescartados) {
            mensaje.append("\n\t\t").append(plato.toString());
        }
        
        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }

}
