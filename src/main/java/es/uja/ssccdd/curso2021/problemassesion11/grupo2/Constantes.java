/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo2;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    // Generador de números aleatorios
    public static final Random aleatorio = new Random();
    
    // Enumerado para el tipo de proceso
    public enum TipoPrioridad {
        BAJA(30), MEDIA(75), ALTA(95), MAXIMA(100);
        
        private final int valor;

        private TipoPrioridad(int valor) {
            this.valor = valor;
        }
        
        /**
         * Obtenemos un tipo de prioridad relacionado con su valor de construcción
         * @param valor, entre 0 y 100, de constucción del componente
         * @return el TipoPrioridad con el valor de construcción
         */
        public static TipoPrioridad getTipoPrioridad(int valor) {
            TipoPrioridad resultado = BAJA;
            TipoPrioridad[] tipo = TipoPrioridad.values();
            int i = 0;
            boolean fin = false;
            
            while( (i < tipo.length) && !fin ) {
                if ( tipo[i].valor >= valor ) {
                    resultado = tipo[i];
                    fin = true;
                }
                
                i++;
            }
            
            return resultado;
        } 
    }
    
    // Constantes del problema
    public static final int NUM_GESTORES = 5;
    public static final int NUM_CREADORES = 3;
    public static final int CAPACIDAD_PROCESOS = 15;
    public static final int VALOR_CONSTRUCCION = 101; // Valor máximo
    public static final int TIPOS_PRIORIDAD = TipoPrioridad.values().length;
    public static final int MINIMO = 2;
    public static final int MAXIMO = 5;
    public static final int INICIO = 0;
    public static final int NO_ASIGNADO = -1;
    public static final int TIEMPO_ESPERA = 1; // expresado en minutos
    public static final int TIEMPO_PROCESO = 1;
    
    // Conexión Broker
    public static final String QUEUE = "uja.ssccdd.sesion11";
    public static final String BROKER_URL = "tcp://suleiman.ujaen.es:8018";
}
