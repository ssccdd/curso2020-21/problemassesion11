/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo1;

import es.uja.ssccdd.curso2021.problemassesion11.grupo1.Constantes.TipoProceso;

/**
 *
 * @author pedroj
 */
public class Proceso {
    private final int iD;
    private final TipoProceso tipoProceso;

    public Proceso(int iD, TipoProceso tipo) {
        this.iD = iD;
        this.tipoProceso = tipo;
    }

    public int getiD() {
        return iD;
    }

    public TipoProceso getTipoProceso() {
        return tipoProceso;
    }

    @Override
    public String toString() {
        return "Proceso{" + "iD=" + iD + ", tipoProceso=" + tipoProceso + '}';
    }
}
