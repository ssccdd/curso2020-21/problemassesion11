/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo6;

import es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.FabricanteVacuna;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.BROKER_URL;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.PACIENTES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.QUEUE;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.TIEMPO_ESPERA_ENFERMERO;
import java.util.concurrent.atomic.AtomicInteger;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Enfermero implements Runnable {

    private final int iD;
    private final AtomicInteger contadorIDs;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public Enfermero(int iD, AtomicInteger contadorIDs) {
        this.iD = iD;
        this.contadorIDs = contadorIDs;
    }

    @Override
    public void run() {

        try {
            before();
            task();
        } catch (Exception e) {
            System.out.println("Enfermero " + iD + " ha encontrado una excepción" + e.getMessage());
        } finally {
            after();
        }
    }

    public void before() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
    }

    public void after() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }

    private void task() throws JMSException {
        MessageConsumer consumer = session.createConsumer(destination);
        ArrayList<Paciente> listaPacientes = new ArrayList<>();
        ArrayList<DosisVacuna> dosisDescartadas = new ArrayList<>();
        boolean interrumpido = false;

        System.out.println("Enfermero " + iD + " ha empezado.");

        for (int i = 0; i < PACIENTES_A_GENERAR; i++) {
            listaPacientes.add(new Paciente(contadorIDs.getAndIncrement()));
        }

        while (!interrumpido) {
            try {

                TextMessage mensaje = (TextMessage) consumer.receive();
                System.out.println("Enfermero recibe: " + mensaje.getText());

                String[] mensajeSplit = mensaje.getText().split("-");
                FabricanteVacuna tipo = FabricanteVacuna.getFabricantePorOrdinal(Integer.parseInt(mensajeSplit[0]));
                int id = Integer.parseInt(mensajeSplit[1]);

                DosisVacuna dosis = new DosisVacuna(id, tipo);

                int i = 0;
                boolean insertado = false;

                while (i < listaPacientes.size() && !insertado) {
                    insertado = listaPacientes.get(i).addDosisVacuna(dosis);
                    i++;
                }

                if (!insertado) {
                    dosisDescartadas.add(dosis);
                }

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_ENFERMERO);
            } catch (InterruptedException | JMSException ex) {
                interrumpido = true;

            }
        }
        
        consumer.close();

        imprimirDatos(listaPacientes, dosisDescartadas);

    }

    private void imprimirDatos(ArrayList<Paciente> listaPacientes, ArrayList<DosisVacuna> dosisDescartadas) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nEnfermero ").append(iD).append(" atendidos ").append(listaPacientes.size()).append(" pacientes.");

        for (Paciente paciente : listaPacientes) {
            mensaje.append("\n\t\t").append(paciente.toString());
        }

        mensaje.append("\n\tDosis descartadas:");
        for (DosisVacuna dosis : dosisDescartadas) {
            mensaje.append("\n\t\t").append(dosis.toString());
        }

        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }
}
