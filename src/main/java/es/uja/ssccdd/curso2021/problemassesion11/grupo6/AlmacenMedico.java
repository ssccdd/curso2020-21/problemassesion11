/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.BROKER_URL;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.QUEUE;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.TIEMPO_ESPERA_ALMACEN;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.FabricanteVacuna;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class AlmacenMedico implements Runnable {
private final int iD;
    private final AtomicInteger contadorIDs;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public AlmacenMedico(int iD, AtomicInteger contadosIDs) {
        this.iD = iD;
        this.contadorIDs = contadosIDs;
    }

    @Override
    public void run() {
        try {
            before();
            task();
        } catch (Exception e) {
            System.out.println("Almacén " + iD + " ha encontrado una excepción" + e.getMessage());
        } finally {
            after();
        }
    }

    public void before() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
    }

    public void after() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }

    private void task() throws JMSException {

        boolean interrumpido = false;
        MessageProducer producer = session.createProducer(destination);
        ArrayList<DosisVacuna> dosisCreadas = new ArrayList<>();
        
        System.out.println("Almacén " + iD + " iniciado.");

        while (!interrumpido) {

            try {

                int idDosis = contadorIDs.getAndIncrement();
                FabricanteVacuna fabricante = FabricanteVacuna.getFabricante(ThreadLocalRandom.current().nextInt(VALOR_GENERACION));
                DosisVacuna dosis = new DosisVacuna(idDosis, fabricante);
                String mensaje = dosis.getFabricante().ordinal() + "-" + dosis.getiD();

                TextMessage message = session.createTextMessage(mensaje);
                producer.send(message);

                dosisCreadas.add(dosis);
              
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_ALMACEN);
            } catch (InterruptedException e) {
                interrumpido = true;
            }

        }
        
        producer.close();

        imprimirDatos(dosisCreadas);
        
    }

    private void imprimirDatos(ArrayList<DosisVacuna> dosis) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nAlmacén ").append(iD).append(" creadas ").append(dosis.size()).append(" dosis.");

        for (DosisVacuna ds : dosis) {
            mensaje.append("\n\t\t").append(ds.toString());
        }
        
        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }

}
