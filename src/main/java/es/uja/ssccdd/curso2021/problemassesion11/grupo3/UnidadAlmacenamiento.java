/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion11.grupo3.Constantes.ESPACIO_INSUFICIENTE;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class UnidadAlmacenamiento {
    private final int iD;
    private final int capacidad;
    private int ocupado;
    private final ArrayList<Archivo> almacenado;

    public UnidadAlmacenamiento(int iD, int capacidad) {
        this.iD = iD;
        this.capacidad = capacidad;
        this.ocupado = 0;
        this.almacenado = new ArrayList();
    }

    public int getiD() {
        return iD;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public int getOcupado() {
        return ocupado;
    }
    
    /**
     * Almacena el archivo en la unidad de almacenamiento si es posible
     * @param archivo que se quiere incluir en la unidad de almacenamiento
     * @return espacio disponible en la unidad de almacenamiento o ESPACIO_INSUFICIENTE
     *         si no se puede atender la solicitud
     */
    public int addArchivo(Archivo archivo) {
        int disponible = capacidad - ocupado;
        int solicitud = archivo.getTipoArchivo().getSize();
        
        if( solicitud <= disponible ) {
            ocupado = ocupado + solicitud;
            almacenado.add(archivo);
            disponible = disponible - solicitud;
        } else 
            disponible = ESPACIO_INSUFICIENTE;
        
        return disponible;
    }
    
    @Override
    public String toString() {
        String resultado = "UnidadAlmacenamiento(" + "iD_" + iD + ") archivos almacenados\n" +
                                "\tCAPACIDAD " + capacidad + "\n";
        
        for(Archivo archivo : almacenado) {
            resultado = resultado + "\t" + archivo + " ocupando " + archivo.getTipoArchivo().getSize() 
                        + " unidades de almacenamiento\n";
        } 
        
        resultado = resultado + "\tESPACIO DISPONIBLE " + (capacidad - ocupado) + "\n";
        
        return resultado;
    }
}
