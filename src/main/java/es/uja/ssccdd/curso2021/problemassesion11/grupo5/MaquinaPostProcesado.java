/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo5;

import es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.TIEMPO_ESPERA_MAQUINA;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.BROKER_URL;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.QUEUE;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.MODELOS_MAXIMOS_POR_CALIDAD;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.TOTAL_CALIDADES;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class MaquinaPostProcesado implements Runnable {

    private final int iD;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public MaquinaPostProcesado(int iD) {
        this.iD = iD;
    }

    @Override
    public void run() {

        try {
            before();
            task();
        } catch (Exception e) {
            System.out.println("Maquina " + iD + " ha encontrado una excepción" + e.getMessage());
        } finally {
            after();
        }
    }

    public void before() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
    }

    public void after() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }

    private void task() throws JMSException {
        MessageConsumer consumer = session.createConsumer(destination);
        ArrayList<ArrayList<Modelo>> listaModelos = new ArrayList<>();
        ArrayList<Modelo> modelosDescartados = new ArrayList<>();
        boolean interrumpido = false;

        System.out.println("Máquina postprocesado " + iD + " ha empezado.");

        for (int i = 0; i < TOTAL_CALIDADES; i++) {
            listaModelos.add(new ArrayList<>());
        }

        while (!interrumpido) {
            try {

                TextMessage mensaje = (TextMessage) consumer.receive();
                System.out.println("Máquina postprocesado recibe: " + mensaje.getText());

                String[] mensajeSplit = mensaje.getText().split("-");
                CalidadImpresion tipo = CalidadImpresion.getCalidadPorOrdinal(Integer.parseInt(mensajeSplit[0]));
                int id = Integer.parseInt(mensajeSplit[1]);

                Modelo modelo = new Modelo(id, tipo);

                if (listaModelos.get(modelo.getCalidadRequeridad().ordinal()).size() < MODELOS_MAXIMOS_POR_CALIDAD) {
                    listaModelos.get(modelo.getCalidadRequeridad().ordinal()).add(modelo);
                } else {
                    modelosDescartados.add(modelo);
                }

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_MAQUINA);
            } catch (InterruptedException | JMSException ex) {
                interrumpido = true;

            }
        }

        consumer.close();
        
        imprimirDatos(listaModelos, modelosDescartados);

    }

    private void imprimirDatos(ArrayList<ArrayList<Modelo>> listaModelos, ArrayList<Modelo> modelosDescartados) {
        StringBuilder mensaje = new StringBuilder();

        int totalProcesados = 0;

        for (int i = 0; i < TOTAL_CALIDADES; i++) {
            totalProcesados += listaModelos.get(i).size();
        }

        mensaje.append("\nMaquina ").append(iD).append(" procesados ").append(totalProcesados).append(" menús.");

        for (int i = 0; i < TOTAL_CALIDADES; i++) {

            mensaje.append("\n\t").append(CalidadImpresion.getCalidadPorOrdinal(i).name()).append(":");
            for (Modelo modelo : listaModelos.get(i)) {
                mensaje.append("\n\t\t").append(modelo.toString());

            }
        }

        mensaje.append("\n\tModelos descartados:");
        for (Modelo modelo : modelosDescartados) {
            mensaje.append("\n\t\t").append(modelo.toString());
        }

        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }

}
