/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo4;

import es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.TipoPlato;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.BROKER_URL;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.QUEUE;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.TIEMPO_ESPERA_RESTAURANTE;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Restaurante implements Runnable {

    private final int iD;
    private final AtomicInteger contIdentificadores;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public Restaurante(int iD, AtomicInteger contIdentificadores) {
        this.iD = iD;
        this.contIdentificadores = contIdentificadores;
    }

    @Override
    public void run() {

        try {
            before();
            task();
        } catch (Exception e) {
            System.out.println("Restaurante " + iD + " ha encontrado una excepción" + e.getMessage());
        } finally {
            after();
        }
    }

    public void before() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
    }

    public void after() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }

    private void task() throws JMSException {

        ArrayList<Plato> platos = new ArrayList<>();
        System.out.println("Restaurante " + iD + " ha empezado.");
        MessageProducer producer = session.createProducer(destination);
        boolean terminar = false;
        
        while (!terminar) {
            try {
                
                int idPlato = contIdentificadores.getAndIncrement();
                TipoPlato tipo = TipoPlato.getPlato(ThreadLocalRandom.current().nextInt(VALOR_GENERACION));
                Plato plato = new Plato(idPlato, tipo);
                String mensaje = plato.getTipo().ordinal() + "-" + plato.getiD();

                TextMessage message = session.createTextMessage(mensaje);
                producer.send(message);

                platos.add(plato);
                
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_RESTAURANTE);

            } catch (InterruptedException ex) {
                terminar = true;
            }
        }
        producer.close();
        imprimirDatos(platos);

    }
    
    private void imprimirDatos(ArrayList<Plato> platos) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nRestaurante").append(iD).append(" generados ").append(platos.size()).append(" platos.");

        for (Plato plato : platos) {
            mensaje.append("\n\t\t").append(plato.toString());
        }
        
        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }

}
