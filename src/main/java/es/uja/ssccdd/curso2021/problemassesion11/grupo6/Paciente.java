/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo6;

import es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.FabricanteVacuna;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo6.Utils.DOSIS_POR_PACIENTE;
import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Paciente{

    private final int iD;
    private FabricanteVacuna tipoVacuna;
    private final ArrayList<DosisVacuna> vacunasInyectadas;

    /**
     * Genera un nuevo paciente
     *
     * @param iD id del paciente
     * @param edad edad del paciente, para ordenar.
     */
    public Paciente(int iD) {
        this.iD = iD;
        this.tipoVacuna = null;
        this.vacunasInyectadas = new ArrayList<>();
    }

    public int getiD() {
        return iD;
    }

    public FabricanteVacuna getTipoVacuna() {
        return tipoVacuna;
    }

    public int getNumeroVacunasInyectadas() {
        return vacunasInyectadas.size();
    }

    public boolean isInmunizado() {
        return vacunasInyectadas.size() == DOSIS_POR_PACIENTE;
    }

    /**
     * Asigna una nueva inyeccion de vacuna al paciente
     *
     * @param vacuna para añadir a la lista si es la primera se
     * @return true si se ha añadido false si no es posible, por ejemplo al
     * mezclar fabricantes adecuadas o si ya se han tomado las dos dosis
     */
    public boolean addDosisVacuna(DosisVacuna vacuna) {
        boolean resultado = false;

        if (vacunasInyectadas.isEmpty()) {

            vacunasInyectadas.add(vacuna);
            tipoVacuna = vacuna.getFabricante();
            resultado = true;

        } else if (vacunasInyectadas.size() < DOSIS_POR_PACIENTE && tipoVacuna.equals(vacuna.getFabricante())) {

            vacunasInyectadas.add(vacuna);
            resultado = true;

        }

        return resultado;
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        StringBuilder resultado = new StringBuilder();

        resultado.append("Paciente[" + iD + ", vacuna=" + tipoVacuna + ", immune='" + (vacunasInyectadas.size() == DOSIS_POR_PACIENTE ? "SI" : "NO") + "']{" + "Dosis=[");

        for (DosisVacuna vacuna : vacunasInyectadas) {
            resultado.append(vacuna.toString() + ", ");
        }

        resultado.delete(resultado.length() - 2, resultado.length()); // para borrar la última coma
        resultado.append("]}");

        return resultado.toString();
    }

}
