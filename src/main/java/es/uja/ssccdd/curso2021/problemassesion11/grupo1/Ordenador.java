/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion11.grupo1.Constantes.MEMORIA_COMPLETA;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo1.Constantes.TIPOS_PROCESO;
import es.uja.ssccdd.curso2021.problemassesion11.grupo1.Constantes.TipoProceso;
import java.util.Arrays;

/**
 *
 * @author pedroj
 */
public class Ordenador {
    private final int iD;
    private final Proceso[][] memoria;

    /**
     * @param iD identificador del ordenador
     * @param capacidad capacidad máxima para cada tipo de proceso que puede
     *                  almacenar en su memoria el ordenador
     */
    public Ordenador(int iD, int[] capacidad) {
        this.iD = iD;
        this.memoria = new Proceso[TIPOS_PROCESO][];
        for(int i = 0; i < TIPOS_PROCESO; i++)
            this.memoria[i] = new Proceso[capacidad[i]];
    }

    public int getiD() {
        return iD;
    }
    
    /**
     * Se añade un proceso a la memoria del ordenador si es posible
     * @param proceso proceso que se añade a la memoria
     * @return la pacacidad que aún queda para almacenar más procesos del mismo
     *         tipo o MEMORIA_COMPLETA
     */
    public int addProceso(Proceso proceso) {
        int resultado = MEMORIA_COMPLETA;
        int tipoProceso = proceso.getTipoProceso().ordinal();
        
        int i = 0;
        boolean asignado = false;
        while( (i < memoria[tipoProceso].length) && !asignado )
            if( memoria[tipoProceso][i] == null ) {
                memoria[tipoProceso][i] = proceso;
                resultado = memoria[tipoProceso].length - i; // capacidad que queda
                asignado = true;
            } else
                i++;
                
        return resultado;
    }

    @Override
    public String toString() {
        String resultado = "Ordenador(" + "iD_" + iD + ") procesos en su memoria\n";
        
        for(int i = 0; i < TIPOS_PROCESO; i++)
            resultado = resultado + "\t" + TipoProceso.values()[i] + "="+ Arrays.toString(memoria[i]) + "\n"; 
        
        return resultado;
    }
}
