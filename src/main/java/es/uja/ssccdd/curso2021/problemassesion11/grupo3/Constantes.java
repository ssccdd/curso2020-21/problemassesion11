/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo3;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    // Generador de números aleatorios
    public static final Random aleatorio = new Random();
    
    // Enumerado para el tipo de proceso
    public enum TipoArchivo {
        PEQUEÑO(25,2), MEDIANO(70,4), GRANDE(95,8), MUY_GRANDE(100,10);
        
        private final int valor;
        private final int size;

        private TipoArchivo(int valor, int size) {
            this.valor = valor;
            this.size = size;
        }
        
        /**
         * Obtenemos un tipo de archivo relacionado con su valor de construcción
         * @param valor, entre 0 y 100, de constucción del componente
         * @return el TipoArchivo con el valor de construcción
         */
        public static TipoArchivo getTipoArchivo(int valor) {
            TipoArchivo resultado = PEQUEÑO;
            TipoArchivo[] tipo = TipoArchivo.values();
            int i = 0;
            boolean fin = false;
            
            while( (i < tipo.length) && !fin ) {
                if ( tipo[i].valor >= valor ) {
                    resultado = tipo[i];
                    fin = true;
                }
                
                i++;
            }
            
            return resultado;
        } 
        
        /**
         * Obtenemos las unidades de almacenamiento que corresponden al tipo 
         * de archivo
         * @return el espacio de almacenamiento en unidades
         */
        public int getSize() {
            return this.size;
        } 
    }
    
    // Constantes del problema
    public static final int NUM_ARCHIVOS = 15;
    public static final int NUM_GESTORES = 5;
    public static final int NUM_CREADORES = 3;
    public static final int VALOR_CONSTRUCCION = 101; // Valor máximo
    public static final int TIPOS_ARCHIVO = TipoArchivo.values().length;
    public static final int MINIMO = 2;
    public static final int MAXIMO = 5;
    public static final int MINIMO_ALMACENAMIENTO = 20;
    public static final int MAXIMO_ALMACENAMIENTO = 50;
    public static final int ESPACIO_INSUFICIENTE = -1;
    public static final int INICIO = 0;
    public static final int TIEMPO_CREAR_ARCHIVO = 2;
    public static final int TIEMPO_ESPERA = 1; // expresado en minutos
    
    // Conexión Broker
    public static final String QUEUE = "uja.ssccdd.sesion11";
    public static final String BROKER_URL = "tcp://suleiman.ujaen.es:8018";
}
