/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo4;

import static es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.REPARTIDORES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo4.Utils.RESTAURANTES_A_GENERAR;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion11 {

    public static void main(String[] args) {

        AtomicInteger contIdPlatos = new AtomicInteger();
        AtomicInteger contIdMenus = new AtomicInteger();
        ExecutorService executor = (ExecutorService) Executors.newCachedThreadPool();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando restaurantes");
        for (int i = 0; i < RESTAURANTES_A_GENERAR; i++) {
            executor.execute(new Restaurante(i, contIdPlatos));
        }

        System.out.println("HILO-Principal Generando repartidores");
        for (int i = 0; i < REPARTIDORES_A_GENERAR; i++) {
            executor.execute(new Repartidor(i, contIdMenus));
        }

        System.out.println("HILO-Principal Espera para parar a los repartidores");

        try {
            TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion11.class.getName()).log(Level.SEVERE, null, ex);
        }

        executor.shutdownNow();

        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion11.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");
        
        
        System.out.println("HILO-Principal Contador ID platos: " + contIdPlatos);
        System.out.println("HILO-Principal Contador ID menús: " + contIdMenus);
        
    }

}
