/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo5;

import es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.BROKER_URL;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.QUEUE;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion11.grupo5.Utils.TIEMPO_ESPERA_INGENIERO;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ingeniero implements Runnable {

    private final int iD;
    private final AtomicInteger contadorIDs;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public Ingeniero(int iD, AtomicInteger contadosIDs) {
        this.iD = iD;
        this.contadorIDs = contadosIDs;
    }

    @Override
    public void run() {

        try {
            before();
            task();
        } catch (Exception e) {
            System.out.println("Ingeniero " + iD + " ha encontrado una excepción" + e.getMessage());
        } finally {
            after();
        }
    }

    public void before() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
    }

    public void after() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }

    private void task() throws JMSException {

        boolean interrumpido = false;
        MessageProducer producer = session.createProducer(destination);
        ArrayList<Modelo> modelosImpresos = new ArrayList<>();
        
        System.out.println("Ingeniero " + iD + " iniciado.");

        while (!interrumpido) {

            try {

                int idModelo = contadorIDs.getAndIncrement();
                CalidadImpresion calidad = CalidadImpresion.getCalidad(ThreadLocalRandom.current().nextInt(VALOR_GENERACION));
                Modelo modelo = new Modelo(idModelo, calidad);
                String mensaje = modelo.getCalidadRequeridad().ordinal() + "-" + modelo.getiD();

                TextMessage message = session.createTextMessage(mensaje);
                producer.send(message);

                modelosImpresos.add(modelo);
              
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_INGENIERO);
            } catch (InterruptedException e) {
                interrumpido = true;
            }

        }

        producer.close();
        
        imprimirDatos(modelosImpresos);
        
    }

    private void imprimirDatos(ArrayList<Modelo> modelos) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nIngeniero ").append(iD).append(" impresos ").append(modelos.size()).append(" modelos.");

        for (Modelo modelo : modelos) {
            mensaje.append("\n\t\t").append(modelo.toString());
        }
        
        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }
    
}
