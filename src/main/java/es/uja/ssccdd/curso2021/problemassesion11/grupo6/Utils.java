/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo6;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Utils {

    
    public static final String QUEUE = "uja.ssccdd.sesion11.connection.alluque";
    public static final String BROKER_URL = "tcp://suleiman.ujaen.es:8018";
    
    // Constantes del problema
    public static final int VALOR_GENERACION = 101; // Valor máximo
    public static final int TOTAL_TIPOS_FABRICANTES = FabricanteVacuna.values().length;
    public static final int TIEMPO_ESPERA_HILO_PRINCIPAL = 15000;
    public static final int ENFERMEROS_A_GENERAR = 5;
    public static final int ALMACENES_A_GENERAR = 3;
    public static final int PACIENTES_A_GENERAR = 5;
    public static final int TIEMPO_ESPERA_ALMACEN = 500;
    public static final int TIEMPO_ESPERA_ENFERMERO = 250;
    public static final int DOSIS_POR_PACIENTE = 2;

    //Enumerado para el fabricante de la vacuna
    public enum FabricanteVacuna {
        PFINOS(33), ANTIGUA(66), ASTROLUNAR(100);

        private final int valor;

        private FabricanteVacuna(int valor) {
            this.valor = valor;
        }

        /**
         * Obtenemos un tipo de vacuna relacionada con su valor de generación
         *
         * @param valor, entre 0 y 100, de generación
         * @return el laboratorio asociado con el valor de generación
         */
        public static FabricanteVacuna getFabricante(int valor) {
            FabricanteVacuna resultado = null;
            FabricanteVacuna[] laboratorios = FabricanteVacuna.values();
            int i = 0;

            while ((i < laboratorios.length) && (resultado == null)) {
                if (laboratorios[i].valor >= valor) {
                    resultado = laboratorios[i];
                }

                i++;
            }

            return resultado;
        }

        /**
         * Obtenemos un tipo de vacuna relacionada con su valor ordinal
         *
         * @param ordinal, entre 0 y (TOTAL_TIPOS_FABRICANTES - 1)
         * @return el laboratorio asociado con el valor ordinal
         */
        public static FabricanteVacuna getFabricantePorOrdinal(int ordinal) {
            return FabricanteVacuna.values()[ordinal];
        }
    }

}
