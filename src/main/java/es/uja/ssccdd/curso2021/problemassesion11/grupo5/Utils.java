/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion11.grupo5;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Utils {

    public static final String QUEUE = "uja.ssccdd.sesion11.connection.alluque";
    public static final String BROKER_URL = "tcp://suleiman.ujaen.es:8018";

    // Constantes del problema
    public static final int VALOR_GENERACION = 101; // Valor máximo
    public static final int TOTAL_CALIDADES = CalidadImpresion.values().length;
    public static final int TIEMPO_ESPERA_HILO_PRINCIPAL = 15000;
    public static final int TIEMPO_ESPERA_INGENIERO = 500;
    public static final int TIEMPO_ESPERA_MAQUINA = 250;
    public static final int INGENIEROS_A_GENERAR = 3;
    public static final int MAQUINAS_A_GENERAR = 5;
    public static final int MODELOS_MAXIMOS_POR_CALIDAD = 4;

    //Enumerado para el tipo de calidad de impresión
    public enum CalidadImpresion {
        MEDICINA(33), TALLER(66), INDUSTRIAL(100);

        private final int valor;

        private CalidadImpresion(int valor) {
            this.valor = valor;
        }

        /**
         * Obtenemos un calidad de impresión relacionada con su valor de
         * generación
         *
         * @param valor, entre 0 y 100, de generación de calidad
         * @return la CalidadImpresión con el valor de generación
         */
        public static CalidadImpresion getCalidad(int valor) {
            CalidadImpresion resultado = null;
            CalidadImpresion[] calidades = CalidadImpresion.values();
            int i = 0;

            while ((i < calidades.length) && (resultado == null)) {
                if (calidades[i].valor >= valor) {
                    resultado = calidades[i];
                }

                i++;
            }

            return resultado;
        }
        
        /**
         * Obtenemos una calidad relacionado con su ordinal
         *
         * @param ordinal, entre 0 y TOTAL_CALIDADES - 1
         * @return el CalidadImpresion con el valor de generación
         */
        public static CalidadImpresion getCalidadPorOrdinal(int ordinal) {
            return CalidadImpresion.values()[ordinal];
        }
    }
}
