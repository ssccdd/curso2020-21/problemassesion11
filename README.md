[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 11

Problemas propuestos para la Sesión11 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2020-21.

Los ejercicios son diferentes para cada grupo:

- [Grupo 1](https://gitlab.com/ssccdd/curso2020-21/problemassesion11#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2020-21/problemassesion11#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2020-21/problemassesion11#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2020-21/problemassesion11#grupo-4)
- [Grupo 5](https://gitlab.com/ssccdd/curso2020-21/problemassesion11#grupo-5)
- [Grupo 6](https://gitlab.com/ssccdd/curso2020-21/problemassesion11#grupo-6)

### Grupo 1
La herramienta de Java para la realización del ejercicio es **JMS**, por medio de una conexión **P2P** y con recepción **síncrona** de los mensajes, y el *broker* [ActiveMQ](http://activemq.apache.org/). Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `Ordenador`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

- `CrearProcesos` : Esta tarea se encargará de ir generando un tipo de proceso y enviar el resultado para que un gestor de memoria pueda asignar ese proceso a un ordenador.

	La tarea que tiene que realizar es:
	
	- Inicia la conexión con el _broker_ con las constantes de configuración que hay en la interface `Constantes`. El destino donde se enviarán los mensajes está definido en la constante `QUEUE`. Además hay que añadir el identificador de ILIAS del alumno.
	- Mientras no se pida la finalización de la tarea:
		- Se genera un tipo de proceso y se simulará un tiempo de creación definido por las constantes.
		- Se envía el mensaje cuyo contenido es el nombre del tipo de proceso.

- `GestorMemoria` : Es el encargado de asignar un `Proceso` a un ordenador de los que tiene asignados. El tipo de proceso lo recogerá de una ubicación establecida.

	La tarea que debe realizar es la siguiente:
	
	- Inicia la conexión con el _broker_ con las constantes de configuración que hay en la interface `Constantes`. El destino donde se recogen los mensajes está definido en la constante `QUEUE`. Además hay que añadir el identificador de ILIAS del alumno.
	- Crea una lista de ordenadores a la que tendrá que asignar los procesos.
	- Se crea una lista para los procesos no asignados.
	- Mientras no se pida la finalización de la tarea:
		- Se obtiene el primer nombre del tipo proceso del destino en el que se encuentran almacenados.
		- Se crea un `Proceso` y se buscará un ordenador donde asignarlo, hay que procurar que la carga de procesos esté balanceada entre los diferentes ordenadores. Se simulará un tiempo aleatorio, atendiendo al tipo de proceso, para la realización de esta operación.
		- Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados.
	- Antes de finalizar debe presentar un informe con los procesos asignados a los ordenadores y los que no hayan podido asignarse.
	
- `Hilo Principal` Deberá completar los siguientes pasos:
	- Se crean un número de tareas para crear procesos determinado por una constante.
	- Crea un número de gestores de memoria que estarán determinados por una constante.
	- Se añaden a un marco de ejecución para que las tareas puedan completarse.
	- Se suspende la ejecución por un `TIEMPO_ESPERA`.
	- Finalizan las tareas activas y espera su finalización para concluir.

### Grupo 2
La herramienta de Java para la realización del ejercicio es **JMS**, por medio de una conexión **P2P** y con recepción **síncrona** de los mensajes, y el *broker* [ActiveMQ](http://activemq.apache.org/). Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `ColaPrioridad`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

- `CrearProcesos` : Esta tarea se encargará de ir generando un tipo de prioridad y enviar el resultado para que un gestor de procesos pueda asignarlo a una cola de prioridad.

	La tarea que tiene que realizar es:
	
	- Inicia la conexión con el _broker_ con las constantes de configuración que hay en la interface `Constantes`. El destino donde se enviarán los mensajes está definido en la constante `QUEUE`. Además hay que añadir el identificador de ILIAS del alumno.
	- Mientras no se pida la finalización de la tarea:
		- Se genera un tipo de prioridad y se simulará un tiempo de creación definido por las constantes.
		- Se envía el mensaje cuyo contenido es el nombre del tipo de prioridad.

- `GestorProcesos` : Es el encargado de asignar un `Proceso` a una de las colas de prioridad. El tipo de prioridad lo recogerá de una ubicación establecida.

	La tarea que debe realizar es la siguiente:
	
	- Inicia la conexión con el _broker_ con las constantes de configuración que hay en la interface `Constantes`. El destino donde se recogen los mensajes está definido en la constante `QUEUE`. Además hay que añadir el identificador de ILIAS del alumno.
	- Crear una lista de colas de prioridad a las que tiene acceso el gestor.
	- Se crea una lista para los procesos no asignados.
	- Mientras no se pida la finalización de la tarea:
		- Se obtiene el primer nombre del tipo de prioridad del destino en el que se encuentran almacenados.
		- Se crea un `Proceso` y se buscará una cola de prioridad donde asignarlo, hay que procurar que la carga de procesos esté balanceada entre las diferentes colas de prioridad. Se simulará un tiempo aleatorio, atendiendo al tipo de proceso, para la realización de esta operación.
		- Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados. 
	- Antes de finalizar debe presentar un informe con los procesos asignados a las colas de prioridad y los que no hayan podido asignarse.

- `Hilo Principal` Deberá completar los siguientes pasos:
	- Se crean un número de tareas para crear procesos determinado por una constante.
	- Crea un número de gestores que estarán determinados por una constante.
	- Se añaden a un marco de ejecución para que las tareas puedan completarse.
	- Se suspende la ejecución por un `TIEMPO_ESPERA`.
	- Finalizan las tareas activas y espera su finalización para concluir.

### Grupo 3
La herramienta de Java para la realización del ejercicio es **JMS**, por medio de una conexión **P2P** y con recepción **síncrona** de los mensajes, y el *broker* [ActiveMQ](http://activemq.apache.org/). Para la realización del ejercicio se hará uso de las clases ya definidas: `Archivo`, `UnidadAlmacenamiento`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

 - `CrearArchivoss` : Esta tarea se encargará de ir creando un tipo de archivo y enviar el resultado para que un gestor de almacenamiento pueda asignarlo a una unidad de almacenamiento.

	La tarea que tiene que realizar es:
	
	- Inicia la conexión con el _broker_ con las constantes de configuración que hay en la interface `Constantes`. El destino donde se enviarán los mensajes está definido en la constante `QUEUE`. Además hay que añadir el identificador de ILIAS del alumno.
	- Mientras no se pida la finalización de la tarea:
		- Se genera un tipo de archivo y se simulará un tiempo de creación definido por las constantes.
		- Se envía el mensaje cuyo contenido es el nombre del tipo de archivo.

- `GestorAlmacenamiento` : Es el encargado de asignar un `Archivo` a una unidad de almacenamiento que tiene asignadas. El tipo de archivo lo recogerá de una ubicación establecida.

	La tarea que debe realizar es la siguiente:
	
	- Inicia la conexión con el _broker_ con las constantes de configuración que hay en la interface `Constantes`. El destino donde se recogen los mensajes está definido en la constante `QUEUE`. Además hay que añadir el identificador de ILIAS del alumno.
	- Crea una lista de unidades de almacenamiento a la que tendrá que asignar los archivos.
	- Crea una lista para añadir los archivos no asignados.
	- Mientras no se no se solicite la interrupción de la tarea:
		- Se obtiene el primer tipo de archivo del destino establecido y se crea su `Archivo` asociado.
		- Para cada `Archivo` se buscará la primera unidad de almacenamiento donde asignarlo. Se simulará un tiempo aleatorio, atendiendo al tipo de archivo, para la realización de esta operación.
		- Si no ha podido ser asignado el archivo se añadirá a la lista de archivos no asignados.
	- Antes de finalizar debe presentar un informe con los archivos asignados a las unidades de almacenamiento y los que no hayan podido asignarse.

- `Hilo Principal` Deberá completar los siguientes pasos:
	- Se crean un número de tareas para crear archivos determinado por una constante.
	- Crea un número de gestores que estarán determinados por una constante.
	- Se añaden a un marco de ejecución para que las tareas puedan completarse.
	- Se suspende la ejecución por un `TIEMPO_ESPERA`.
	- Finalizan las tareas activas y espera su finalización para concluir.

### Grupo 4
Se ha modificado la estructura de la empresa y se han separado físicamente los restaurantes de los repartidores. Los `restaurantes` generarán `platos` necesarios, para que posteriormente los `repartidores` puedan recoger esos platos y rellenar `MenuReparto`. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de  `Executor` que permita generar tantos hilos como sea necesario y **JMS**, por medio de una conexión **P2P** y con recepción **síncrona** de los mensajes, y el _broker_ [ActiveMQ](http://activemq.apache.org/) para que gestione los platos que genera cada restaurante y los sirva a los repartidores que esperan.

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay constantes no incluidas que se deben completar.
- `Plato`: Clase que representa a un plato de tipo `TipoPlato`.
- `MenuReparto`: Representa a un pedido, puede incluir un plato de cada uno de los presentes en `TipoPlato`, pero no más de uno de cada tipo.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `Restaurante`:
	- Los restaurantes guardarán en variables de instancia, su identificador, una variable atómica compartida para asignar IDs diferentes y las variables necesarias para el envío de mensajes. 
	- Cada restaurante fabricará platos hasta que sea interrumpido, cada uno necesita un ID único.
	- Para simular el envío del plato, se enviará a un buzón una cadena de texto con el ordinal del tipo de plato y el identificador separado por un guión, "X-Y", por ejemplo un postre con id 134: "2-134".
	- Entre la generación de cada plato, el restaurante esperará 500 milisegundos.
	- Al finalizar el restaurante imprimirá el detalle de los platos generados.
- `Repartidor`:
	- El repartidor guardará en variables de instancia, su identificador, una variable atómica compartida para asignar IDs diferentes y las variables necesarias para el envío de mensajes. 
	- Nada más empezar generará 5 menús con id único, para ir insertando los platos.
	- El repartidor estará esperando platos de forma indefinida, hasta que sea interrumpido. Para obtener los platos usará el contenido del mensaje para crear un plato, .split() permite dividir una cadena por un carácter concreto y Integer.parseInt() obtiene un entero a partir de un string.
	- El repartidor intentará insertar el plato en los menús, priorizando que los ménus queden completos, si no se puede introducir lo guardará en un array de platos rechazados.
	- Entre el procesamiento de cada plato, el restaurante esperará 250 milisegundos.
	- Al finalizar, imprimirá el detalle de los menús que ha generado y de los platos rechazados.
- `Hilo Principal`:
	- Generará el gestor/ejecutor de tareas.
	- Creará e iniciará la ejecución de 3 restaurantes usando un gestor de hilos.
	- Creará e iniciará la ejecución de 5 repartidores usando un gestor de hilos.
	- Tras esperar 15 segundos interrumpe a todos los procesos.

### Grupo 5
Se ha redistribuido las instalaciones de la empresa y se han separado fisicamente las impresoras e ingenieros de las máquinas de postprocesado. Los `ingenieros` generan modelos impresos, para que posteriormente `MáquinaPostprocesado` reciban y procesen los modelos. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de  `Executor` que permita generar tantos hilos como sea necesario y **JMS**, por medio de una conexión **P2P** y con recepción **síncrona** de los mensajes, y el _broker_ [ActiveMQ](http://activemq.apache.org/) para que gestione los modelos que imprime cada iingeniero y los sirva a las máquinas que esperan.

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay constantes no incluidas que se deben completar.
- `Modelo`: Clase que representa a un modelo 3D con unos requisitos de tipo `CalidadImpresion`.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `Ingeniero`:
	- Los ingenieros guardarán en variables de instancia, su identificador, una variable atómica compartida para asignar IDs diferentes y las variables necesarias para el envío de mensajes.
	- Cada ingeniero imprimirá modelos hasta que sea interrumpido, cada uno necesita un ID único.
	- Para simular el envío del modelo, se enviará a un buzón una cadena de texto con el ordinal de calidad del modelo y el identificador separado por un guión, "X-Y", por ejemplo un modelo médico con id 134: "0-134".
	- Entre la generación de cada modelo, el ingeniero esperará 500 milisegundos.
	- Al finalizar el ingeniero imprimirá el detalle de los modelos generados.
 - `MaquinaPostprocesado`:
   	-  La máquina guardará en variables de instancia su identificador y las variables necesarias para el envío de mensajes. 
	- La máquina estará esperando modelos de forma indefinida, hasta que sea interrumpido. Para obtener los modelos usará el contenido del mensaje para crear un modelo, .split() permite dividir una cadena por un carácter concreto y Integer.parseInt() obtiene un entero a partir de un string.
	- La máquina intentará guardar el modelo, pero solo puede almacenar 4 modelos de cada tipo, si no se puede guardar lo insertará en un array de modelos rechazados.
	- Entre el procesamiento de cada modelo, la máquina esperará 250 milisegundos.
	- Al finalizar, imprimirá los detalles de los modelos que ha guardado de cada tipo y de los modelos rechazados.
- `Hilo Principal`:
	- Generará el gestor/ejecutor de tareas.
	- Creará e iniciará la ejecución de 3 ingenieros usando un gestor de hilos.
	- Creará e iniciará la ejecución de 5 máquinas usando un gestor de hilos.
	- Tras esperar 15 segundos interrumpe a todos los procesos.

### Grupo 6
Los `Almacénes médicos` generarán `DosisVacuna` necesarios, para que posteriormente los `Enfermeros` puedan recoger esas dosis e inyectarlas a `Pacientes`. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de  `Executor` que permita generar tantos hilos como sea necesario y **JMS**, por medio de una conexión **P2P** y con recepción **síncrona** de los mensajes, y el _broker_ [ActiveMQ](http://activemq.apache.org/) para que gestione las dosis que genera cada almacén y los sirva a los enfermeros que esperan.

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay constantes no incluidas que se deben completar.
- `DosisVacuna`: Clase que representa a cada dosis de la vacuna de un `FabricanteVacuna` determinado.
- `Paciente`: Representa a un paciente al que se le pueden administrar hasta dos dosis siempre que las dos sean del mismo fabricante.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `AlmacénMédico`:
	- Los almacenes guardarán en variables de instancia, su identificador, una variable atómica compartida para asignar IDs diferentes y las variables necesarias para el envío de mensajes. 
	- Cada almacén fabricará dosis hasta que sea interrumpido, cada una necesita un ID único.
	- Para simular el envío de la dosis, se enviará a un buzón una cadena de texto con el ordinal del fabricante y el identificador separado por un guión, "X-Y", por ejemplo una dosis de Antigua con id 134: "1-134".
	- Entre la generación de cada dosis, el almacén esperará 500 milisegundos.
	- Al finalizar el almacén imprimirá el detalle de las dosis generados.
- `Enfermero`:
	- El enfermero guardará en variables de instancia, su identificador, una variable atómica compartida para asignar IDs diferentes y las variables necesarias para el envío de mensajes. 
	- Nada más empezar generará 5 pacientes con id único, para vacunar según lleguen las dosis.
	- El enfermero estará esperando dosis de forma indefinida, hasta que sea interrumpido. Para obtener las dosis usará el contenido del mensaje para crear una dosis, .split() permite dividir una cadena por un carácter concreto y Integer.parseInt() obtiene un entero a partir de un string.
	- El enfermero intentará inyectar la dosis en los pacientes, priorizando que los pacientes queden inmunizados, si no se puede inyectar la guardará en un array de dosis rechazadas.
	- Entre el procesamiento de cada dosis, el enfermero esperará 250 milisegundos.
	- Al finalizar, imprimirá el detalle de los pacientes atendidos y de las dosis rechazadas.
- `Hilo Principal`:
	- Generará el gestor/ejecutor de tareas.
	- Creará e iniciará la ejecución de 3 almacenes usando un gestor de hilos.
	- Creará e iniciará la ejecución de 5 enfermeros usando un gestor de hilos.
	- Tras esperar 15 segundos interrumpe a todos los procesos.